#!/bin/sh

rm -rf  build  dist  *.egg-info
python setup.py build develop sdist --formats=zip
twine upload dist/*.zip

